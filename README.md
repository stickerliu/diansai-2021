# 电赛2021

## 介绍
用于电赛资料的分享，代码的备份
## git使用

Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
### 下载到本地

1、新建文件夹，进入文件夹，输入初始化指令
> git init 

2、pull下来
> git pull https://gitee.com/stickerliu/diansai-2021

3、新建远程链接
> git remote add origin https://gitee.com/stickerliu/diansai-2021

如果打错可用`git remote remove [名称]`去掉之后重新添加，查看可用`git remote -v`

### 上传到仓库

1、添加修改的文件
> 单个文件:git add [文件路径加文件名字]

> 所有有改动的文件： git add .

2、添加修改描述
> git commit -m "修改的描述"

3、绑定远程分支(可选)

>  git push --set-upstream origin master

> > 将远程分支中的master分支设置成默认上传分支

4、上传

> git push [路径名] [分支]

> git push origin master

> > 若是进行了远程分支绑定（步骤3），只需要输入`git push`

`origin`是上一步建立远程连接的名字，推荐用`origin`命名

### 演示

![pic/1.png](pic/1.png)

### 建议
1、安装`git`的时候安装带`MinGW`的，在`vscode`中可将终端添加到编辑器下方，方便输入命令

![pic/2.png](pic/2.png)

2、如果说来到桌面路径操作麻烦的话可以建立快捷指令
> vim /etc/profile

在里面添加指令
> alias desktop ‘c/User/......到桌面的路径’

![pic/3.png](pic/3.png)

效果

![pic/4.png](pic/4.png)

3、`push`之前为确保本地文件为最新建议先`pull`一下

4、遇到服务器版本与本地版本不同时，采用：
> git branch --set-upstream-to=origin/master master

教程：
![pic/5.png](pic/5.png)
![pic/6.png](pic/6.png)