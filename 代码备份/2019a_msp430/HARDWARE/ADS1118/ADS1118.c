#include "ADS1118.h"

#define INPUT  	PAin(4)				// 数据输入 , 连接芯片的OUT		
#define OUTPUT	PAout(5)			// 数据输出 , 连接芯片的DIN
#define CS  	  PAout(6)			// 片选信号
#define ClK    	PAout(7)			// 时钟信号

void ADS1118GPIOInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;		// PA6作为片选输出信号，设置为推挽输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ;
	GPIO_Init(GPIOA , &GPIO_InitStructure) ;
	GPIO_SetBits(GPIOA,GPIO_Pin_6);							// 片选初始化为高
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;		// PA5作为数据输出信号，初始设置为推挽输出		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 ;
	GPIO_Init(GPIOA , &GPIO_InitStructure) ;
	GPIO_ResetBits(GPIOA,GPIO_Pin_5);						// 数据输出口初始化为低
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;		// PA7作为时钟信号，设置为推挽输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 ;
	GPIO_Init(GPIOA , &GPIO_InitStructure) ;
	GPIO_ResetBits(GPIOA,GPIO_Pin_7);						// 时钟初始化为低
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;		// PA1作为片选1输出信号，设置为推挽输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ;
	GPIO_Init(GPIOA , &GPIO_InitStructure) ;
	GPIO_SetBits(GPIOA,GPIO_Pin_1);							// 片选初始化为高
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD ;			// PA4作为数据输入信号，设置为下拉输入
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 ;
	GPIO_Init(GPIOA , &GPIO_InitStructure) ;
	
}

char ADS1118_Init(void)
{
  unsigned short REG_Init = CH0 | V6_144 | MODE_SINGLE | RATE_64SPS | TEMPERATURE | PULL_UP_EN | VALID_SET;
  short val=0;
	char ret=0;
	
	ADS1118GPIOInit();
  ret = ADS1118_ReadWrite(REG_Init, &val);
	
  if(ret == 0)
    return 0;
  else
    return 1;
}

char ADS1118_Get_CH0(float* Value)
{
  short val;
  unsigned short REG_CH0_RUN = SET_SS | CH0 | V6_144 | MODE_SINGLE | RATE_64SPS | GET_ADC | PULL_UP_EN | VALID_SET; //设置转换需要的寄存器值
  char ret = ADS1118_ReadWrite(REG_CH0_RUN, &val); //写入寄存器值随即开始转换
  delay_ms(20);//等待转换完成
  ret += ADS1118_ReadWrite(REG_CH0_RUN, &val); //读取转换的数据
  *Value = val * 187.5 / 1000000;
  return ret;
}

char ADS1118_Get_CH1(float* Value)
{
  short val;
  unsigned short REG_CH1_RUN = SET_SS | CH1 | V6_144 | MODE_SINGLE | RATE_64SPS | GET_ADC | PULL_UP_EN | VALID_SET;
  char ret = ADS1118_ReadWrite(REG_CH1_RUN, &val);
  delay_ms(20);
  ret += ADS1118_ReadWrite(REG_CH1_RUN, &val);
  *Value = val * 187.5 / 1000000;
  return ret;
}

char ADS1118_Get_CH2(float* Value)
{
  short val;
  unsigned short REG_CH2_RUN = SET_SS | CH2 | V6_144 | MODE_SINGLE | RATE_64SPS | GET_ADC | PULL_UP_EN | VALID_SET;
  char ret = ADS1118_ReadWrite(REG_CH2_RUN, &val);
	delay_ms(20);
  ret += ADS1118_ReadWrite(REG_CH2_RUN, &val);
  *Value = val * 187.5 / 1000000;
  return ret;
}

char ADS1118_Get_CH3(float* Value)
{
  short val;
  unsigned short REG_CH3_RUN = SET_SS | CH3 | V6_144 | MODE_SINGLE | RATE_64SPS | GET_ADC | PULL_UP_EN | VALID_SET;
  char ret = ADS1118_ReadWrite(REG_CH3_RUN, &val);
  delay_ms(20);
  ret += ADS1118_ReadWrite(REG_CH3_RUN, &val);
  *Value = val * 187.5 / 1000000;

  return ret;
}

char ADS1118_Get_Temperature(float* Temperature)
{
  short val;
  unsigned short REG_TEMP_RUN = SET_SS | V6_144 | MODE_SINGLE | RATE_64SPS | TEMPERATURE | PULL_UP_EN | VALID_SET;
  char ret = ADS1118_ReadWrite(REG_TEMP_RUN, &val);
  delay_ms(100);
  ret += ADS1118_ReadWrite(REG_TEMP_RUN, &val);
  val = ((unsigned short)val) >> 2;//温度数据高十四位有效

  if(val & 0x2000) //温度数据第十四位表明是否为负数
    {
      unsigned short temp = ((unsigned short)val) ^ 0x2000;//第十四位清零
      temp = temp - 1; //转换为反码
      temp = (~temp) & 0x1FFF; //低十三位按位取反得原码
      val = -temp; //重新取负转换为十六位的补码格式

    }

  *Temperature = val * 0.03125f; //乘以温度比例系数
  return ret;
}

char ADS1118_ReadWrite(unsigned short data, short* val)
{
  unsigned int buf=0;
	unsigned char i=0;
  unsigned short temp = data,Config=0;

  for(i = 0; i < 32; i++)
    {

      if(0x8000 & temp) //前十六位写入寄存器值 后十六位写零
        {
          OUTPUT=1;
        }
      else
        {
          OUTPUT=0;
        }

      temp = (temp << 1);
      delay_ms(1);
      ClK=1;

      delay_ms(1);
      if(INPUT==1) //读取数据
        {
          buf |= 0x01;
        }
			buf = buf << 1;
				
      delay_ms(1);
      ClK=0;
      delay_ms(1);
    }

  *val = (short)((unsigned int)(buf & 0xffff0000) >> 16); //读取到的高十六位为ADC数值
  Config = buf & 0xffff; //后16位为刚才写入的寄存器值

  if((data | 0x01) == Config) //设置寄存器最低位读取出来始终为1
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

