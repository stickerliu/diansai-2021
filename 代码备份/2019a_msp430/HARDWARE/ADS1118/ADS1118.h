#ifndef __ADS1118_H_
#define __ADS1118_H_

#include "sys.h"
#include "delay.h"

#define SET_SS 		0x8000           	//单次采集模式开始采集

//差分模式采集通道
#define CH_0_1	    0X0000				// AINP 为 CH0  AINN 为 CH1
#define CH_0_3		0X1000				// AINP 为 CH0  AINN 为 CH3
#define CH_1_3		0X2000				// AINP 为 CH1  AINN 为 CH3
#define CH_2_3		0X3000				// AINP 为 CH2  AINN 为 CH3

//单端模式采集通道
#define CH0 		0x4000           	// AINP 为 CH0  AINN 为 GND
#define CH1 		0x5000				// AINP 为 CH1  AINN 为 GND
#define CH2 		0x6000				// AINP 为 CH2  AINN 为 GND
#define CH3 		0x7000				// AINP 为 CH3  AINN 为 GND

//正负量程
#define V6_144 		0x0000           	// ±6.144V
#define V4_096 		0x0200				// ±4.096V
#define V2_048 		0x0400				// ±2.048V
#define V1_024 		0x0600				// ±1.024V
#define V0_512 		0x0800				// ±0.512V
#define V0_256 		0x0A00				// ±0.256V

#define MODE_SINGLE			0x0100      //单次采集模式
#define MODE_CONTINUE		0x0000		//连续采集模式

//采集速率
#define RATE_8SPS	0x0000           	// 000 = 8SPS
#define RATE_16SPS	0x0020				// 001 = 16SPS
#define RATE_32SPS	0x0040				// 010 = 32SPS
#define RATE_64SPS	0x0060				// 011 = 64SPS
#define RATE_128SPS	0x0080				// 100 = 128SPS
#define RATE_250SPS	0x00A0				// 101 = 250SPS
#define RATE_475SPS	0x00C0				// 110 = 475SPS
#define RATE_860SPS	0x00E0				// 111 = 860SPS

#define TEMPERATURE 0x0010           	//采集内部温度
#define GET_ADC		0x0000           	//采集外部电压

#define PULL_UP_EN	0x0008				//内部上拉DOUT

#define VALID_SET   0x0002 				//此次设置生效
#define INVALID_SET 0x0006 				//此次设置无效

#define POWERON_DEFAULT		0x058A 	 	//芯片开机后的默认初始值

char ADS1118_Init(void);
char ADS1118_Get_CH0(float* Value);
char ADS1118_Get_CH1(float* Value);
char ADS1118_Get_CH2(float* Value);
char ADS1118_Get_CH3(float* Value);
char ADS1118_Get_Temperature(float* Temperature);
char ADS1118_ReadWrite(unsigned short data, short* val);

#endif

