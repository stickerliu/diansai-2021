#include "ADS1118.h"

#define SET_SS 		0x8000           	//单次采集模式开始采集

										//差分模式采集通道
#define CH_0_1	    0X0000				// AINP 为 CH0  AINN 为 CH1
#define CH_0_3		0X1000				// AINP 为 CH0  AINN 为 CH3
#define CH_1_3		0X2000				// AINP 为 CH1  AINN 为 CH3
#define CH_2_3		0X3000				// AINP 为 CH2  AINN 为 CH3

										//单端模式采集通道
#define CH0 		0x4000           	// AINP 为 CH0  AINN 为 GND
#define CH1 		0x5000				// AINP 为 CH1  AINN 为 GND
#define CH2 		0x6000				// AINP 为 CH2  AINN 为 GND
#define CH3 		0x7000				// AINP 为 CH3  AINN 为 GND

										//正负量程
#define V6_144 		0x0000           	// ±6.144V
#define V4_096 		0x0200				// ±4.096V
#define V2_048 		0x0400				// ±2.048V
#define V1_024 		0x0600				// ±1.024V
#define V0_512 		0x0800				// ±0.512V
#define V0_256 		0x0A00				// ±0.256V

#define MODE_SINGLE			0x0100      //单次采集模式
#define MODE_CONTINUE		0x0000		//连续采集模式

										//采集速率
#define RATE_8SPS	0x0000           	// 000 = 8SPS
#define RATE_16SPS	0x0020				// 001 = 16SPS
#define RATE_32SPS	0x0040				// 010 = 32SPS
#define RATE_64SPS	0x0060				// 011 = 64SPS
#define RATE_128SPS	0x0080				// 100 = 128SPS
#define RATE_250SPS	0x00A0				// 101 = 250SPS
#define RATE_475SPS	0x00C0				// 110 = 475SPS
#define RATE_860SPS	0x00E0				// 111 = 860SPS

#define TEMPERATURE 0x0010           	//采集内部温度
#define GET_ADC		0x0000           	//采集外部电压

#define PULL_UP_EN	0x0008				//内部上拉DOUT

#define VALID_SET   0x0002 				//此次设置生效
#define INVALID_SET 0x0006 				//此次设置无效

#define POWERON_DEFAULT		0x058A 	 	//芯片开机后的默认初始值

char ADS1118_Init(void)
{
	unsigned short REG_Init = CH0|V6_144|MODE_SINGLE|RATE_64SPS|TEMPERATURE|PULL_UP_EN|VALID_SET;
	short val;
	char ret = ADS1118_ReadWrite(REG_Init,&val);
	if(ret == 0)
		return 0;
	else
		return -1;
}

char ADS1118_Get_CH0(float* Value){
	short val;
	unsigned short REG_CH0_RUN = SET_SS|CH0|V6_144|MODE_SINGLE|RATE_64SPS|GET_ADC|PULL_UP_EN|VALID_SET; //设置转换需要的寄存器值
	char ret = ADS1118_ReadWrite(REG_CH0_RUN,&val); //写入寄存器值随即开始转换
	HAL_Delay(20);//等待转换完成
	ret += ADS1118_ReadWrite(REG_CH0_RUN,&val); //读取转换的数据
	*Value = val*187.5/1000000;
	return ret;
}

char ADS1118_Get_CH1(float* Value){
	short val;
	unsigned short REG_CH1_RUN = SET_SS|CH1|V6_144|MODE_SINGLE|RATE_64SPS|GET_ADC|PULL_UP_EN|VALID_SET;
	char ret = ADS1118_ReadWrite(REG_CH1_RUN,&val);
	HAL_Delay(20);
	ret += ADS1118_ReadWrite(REG_CH1_RUN,&val);
	*Value = val*187.5/1000000;
	return ret;
}

char ADS1118_Get_CH2(float* Value){
	short val;
	unsigned short REG_CH2_RUN = SET_SS|CH2|V6_144|MODE_SINGLE|RATE_64SPS|GET_ADC|PULL_UP_EN|VALID_SET;
	char ret = ADS1118_ReadWrite(REG_CH2_RUN,&val);
	HAL_Delay(20);
	ret += ADS1118_ReadWrite(REG_CH2_RUN,&val);
	*Value = val*187.5/1000000;
	return ret;
}

char ADS1118_Get_CH3(float* Value){
	short val;
	unsigned short REG_CH3_RUN = SET_SS|CH3|V6_144|MODE_SINGLE|RATE_64SPS|GET_ADC|PULL_UP_EN|VALID_SET;
	char ret = ADS1118_ReadWrite(REG_CH3_RUN,&val);
	HAL_Delay(20);
	ret += ADS1118_ReadWrite(REG_CH3_RUN,&val);
	*Value = val*187.5/1000000;

	return ret;
}

char ADS1118_Get_Temperature(float* Temperature){
	short val;
	unsigned short REG_TEMP_RUN = SET_SS|V6_144|MODE_SINGLE|RATE_64SPS|TEMPERATURE|PULL_UP_EN|VALID_SET;
	char ret = ADS1118_ReadWrite(REG_TEMP_RUN,&val);
	HAL_Delay(100);
	ret += ADS1118_ReadWrite(REG_TEMP_RUN,&val);
	val = ((unsigned short)val) >> 2;//温度数据高十四位有效
	if(val & 0x2000){//温度数据第十四位表明是否为负数
		unsigned short temp = ((unsigned short)val) ^ 0x2000;//第十四位清零
		temp = temp-1;//转换为反码
		temp = (~temp)&0x1FFF; //低十三位按位取反得原码
		val = -temp; //重新取负转换为十六位的补码格式

	}
	*Temperature = val*0.03125f; //乘以温度比例系数
	return ret;
}

char ADS1118_ReadWrite(unsigned short data, short* val)
{
	unsigned int Din;
	unsigned short temp=data;
	for(unsigned char i=0;i<32;i++)
	{
		Din = Din<<1;
		if(0x8000&temp){ //前十六位写入寄存器值 后十六位写零
			HAL_GPIO_WritePin(ADC_IN_GPIO_Port, ADC_IN_Pin, 1);
		}
		else{
			HAL_GPIO_WritePin(ADC_IN_GPIO_Port, ADC_IN_Pin, 0);
		}
		temp = (temp<<1);
		//HAL_Delay(1);
		HAL_GPIO_WritePin(ADC_CLK_GPIO_Port, ADC_CLK_Pin, 1);//拉高CLK
		//HAL_Delay(1);
		if(HAL_GPIO_ReadPin(ADC_OUT_GPIO_Port,ADC_OUT_Pin)){ //读取数据
			Din |= 0x01;
		}
		//HAL_Delay(1);
		HAL_GPIO_WritePin(ADC_CLK_GPIO_Port, ADC_CLK_Pin, 0);//拉低CLK
		//HAL_Delay(1);
	}
	*val = (short)((unsigned int)(Din&0xffff0000)>>16);   //读取到的高十六位为ADC
    unsigned short Config = Din&0xffff; //后16位为刚才写入的寄存器值
	if((data|0x01) == Config){ //设置寄存器最低位读取出来始终为1
		return 0;
	}
	else{
		return -1;
	}
}

