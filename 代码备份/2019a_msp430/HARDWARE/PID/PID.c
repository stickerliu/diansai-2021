#include "pid.h"
#include "pwm.h"
#include "delay.h"

/*
encoder_l:左轮实际数据
encoder_r:右轮实际数据
*/
int encoder_l=0, encoder_r=0;

/*
motor_l:存储左轮输出值
motor_r:存储右轮输出值
*/
int duty_l, duty_r;

/*
target_lu:存储左轮目标值
target_rd:存储右轮目标值
*/
int target_l, target_r;

//速度控制PID参数
float Velocity_KP = 0.01, Velocity_KI = 0;

//上位机调试用、速度的和、速度的差
u8 niming[10] = {0xaa, 0xff, 0xf1, 0x04, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

void PID_Init(void) //TIM6
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE); //使能TIM6时钟

  NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;			  //TIM6中断
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2; //先占优先级0级
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		  //从优先级0级
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  //IRQ通道被使能
  NVIC_Init(&NVIC_InitStructure);							  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

  TIM_TimeBaseStructure.TIM_Period = 499;						//设定计数器自动重装值
  TIM_TimeBaseStructure.TIM_Prescaler = 7199;					//预分频器 50ms
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;				//设置时钟分割:TDTS = Tck_tim
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数模式
  TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);				//根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

  TIM_Cmd(TIM6, ENABLE);					   //开启定时器
  TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE); //开启定时器更新中断
}

int Positional_PID(int En, int Tar)
{
  static int Bias = 0, Pwm = 0, integral = 0;
  Bias = Tar - En; 	         //计算偏差
  integral += Bias;
  Pwm += Velocity_KP * Bias +  Velocity_KI * integral; //位置式PID控制器
  return Pwm;												 //输出
}

//定时器6中断服务程序
void TIM6_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM6, TIM_IT_Update) == SET)   //溢出中断
    {

      //PID速度修正
      duty_l = Positional_PID(encoder_l, target_l);
			duty_r = Positional_PID(encoder_r, target_r);
			
			encoder_l= duty_l;
			encoder_r= duty_r;

      Xianfu_Pwm();                                      			//PWM限幅
      Set_PWM(duty_l, duty_r);

//      //上位机数据赋值
////      niming[4] = (target_lu*33/100);
////      niming[5] = (target_lu*33/100) >> 8;
////			niming[6] = (encoder_lu);
////      niming[7] = (encoder_lu) >> 8;

////			niming[4] = (encoder_lu);
////      niming[5] = (encoder_ru);
////			niming[6] = (encoder_ld);
////      niming[7] = (encoder_rd);

    }

  TIM_ClearITPendingBit(TIM6, TIM_IT_Update); //清除中断标志位
}

//PWM限幅
void Xianfu_Pwm(void)
{
  int Amplitude = 700;

  if(duty_l < -Amplitude) duty_l = -Amplitude;

  if(duty_l > Amplitude)  duty_l = Amplitude;

  if(duty_r < -Amplitude) duty_r = -Amplitude;

  if(duty_r > Amplitude)  duty_r = Amplitude;
}


