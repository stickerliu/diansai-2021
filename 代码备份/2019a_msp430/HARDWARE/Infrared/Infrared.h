#ifndef __INFRARED_H
#define __INFRARED_H

#include "sys.h"
#include "delay.h"

#define infrared_left  PBin(12)   	
#define infrared_right PBin(13)	

#define mid  1
#define lef  2
#define llef 3
#define rig  4
#define rrig 5
#define inp  6

#define fahui 1
#define jichu 0

void infrared_init(void);
u8 infrared_scan(u8 mode);

#endif
