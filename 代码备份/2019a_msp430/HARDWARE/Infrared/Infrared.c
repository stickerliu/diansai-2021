#include "infrared.h"

void infrared_init(void)
{

  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); //使能PORTB时钟

  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_12 | GPIO_Pin_13;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
  GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化

}

//mid  1 正中间
//lef  2 左出界
//llef 3 左大偏
//rig  4 右出界
//rrig 5 右大偏
//inp  6 检测到点

u8  infrared_scan(u8 mode)
{
  static u8 ret = 0,l_flag=0,r_flag=0;

	if(infrared_left == 1 && infrared_right == 1) //未踩到线
    {
      delay_ms(10);

      if(infrared_left == 1 && infrared_right == 1)
        {
          ret = mid;
					l_flag=0;
					r_flag=0;
        }

    }
	
  if(infrared_left == 1 && infrared_right == 0) //左踩到线，车偏右
    {
      delay_ms(10);

      if(infrared_left == 1 && infrared_right == 0)
        {
          ret = rig;
					l_flag=0;
					r_flag=1;
        }

    }

  if(infrared_right == 1 && infrared_left == 0) //右踩到线，车偏左
    {
      delay_ms(10);

      if(infrared_right == 1 && infrared_left == 0)
        {
          ret = lef;
					l_flag=1;
					r_flag=0;
        }
    }
		
	 if(infrared_right == 0 && infrared_left == 0) //右踩到线，车偏左
    {
      delay_ms(10);

      if(infrared_right == 0 && infrared_left == 0 && l_flag==1 )
        {
          ret = llef;
        }
				
			if(infrared_right == 0 && infrared_left == 0 && r_flag==1)
        {
          ret = rrig;
        }
    }

  return ret;
}

