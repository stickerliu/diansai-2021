#ifndef __PWM_H
#define __PWM_H
#include "sys.h"
#include "delay.h"

/*
PE9  - TIM1_CH1 - pwm1
PE11 - TIM1_CH2 - pwm2
PE13 - TIM1_CH3 - pwm3
PE14 - TIM1_CH4 - pwm4

PC6  - TIM8_CH1 - pwm5
PC7  - TIM8_CH2 - pwm6
PC8  - TIM8_CH3 - pwm7
PC9  - TIM8_CH4 - pwm8
*/

#define PWMA1 TIM1->CCR1 //��
#define PWMA2 TIM1->CCR2

#define PWMB1 TIM1->CCR3 //��
#define PWMB2 TIM1->CCR4

#define PWMC1 TIM8->CCR1
#define PWMC2 TIM8->CCR2

#define PWMD1 TIM8->CCR3
#define PWMD2 TIM8->CCR4

void Tim1_Init(u16 arr, u16 psc);
void Tim8_Init(u16 arr, u16 psc);
void PWM_Init(u16 pwm1, u16 pwm2, u16 pwm3, u16 pwm4, u16 pwm5, u16 pwm6, u16 pwm7, u16 pwm8);
void Set_PWM(int left, int right);

#endif
