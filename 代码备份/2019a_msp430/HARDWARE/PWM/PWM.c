#include "PWM.h"

//arr：自动重装值
//psc：时钟预分频数

void Tim1_Init(u16 arr, u16 psc)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);  //使能 TIM1 外设
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE); //使能 PE 时钟
  TIM_DeInit(TIM1);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_FullRemap_TIM1, ENABLE); //全部重映射

  //设置该引脚为复用输出功能,输出 TIM1 的 PWM 脉冲波形
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用功能输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOE, &GPIO_InitStructure); //初始化 GPIO

  TIM_TimeBaseStructure.TIM_Period = arr;						//设置自动重装载周期值
  TIM_TimeBaseStructure.TIM_Prescaler = psc;					//设置预分频值 不分频
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;				//设置时钟分割:TDTS = Tck_tim
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //向上计数
  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);				//初始化 TIM1

  TIM_ARRPreloadConfig(TIM1, ENABLE); //使能 TIM1 在 ARR 上的预装载寄存器
  TIM_CtrlPWMOutputs(TIM1, ENABLE);	//MOE 主输出使能,TIM1高级定时器必须开启
  TIM_Cmd(TIM1, ENABLE);				//使能 TIM1
}

void Tim8_Init(u16 arr, u16 psc)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);  //使能 TIM8 外设
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); //使能 PC 时钟
  TIM_DeInit(TIM8);

  //设置该引脚为复用输出功能,输出 TIM8 的 PWM 脉冲波形
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用功能输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure); //初始化 GPIO

  TIM_TimeBaseStructure.TIM_Period = arr;						//设置自动重装载周期值
  TIM_TimeBaseStructure.TIM_Prescaler = psc;					//设置预分频值 不分频
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;				//设置时钟分割:TDTS = Tck_tim
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //向上计数
  TIM_TimeBaseInit(TIM8, &TIM_TimeBaseStructure);				//初始化 TIM1

  TIM_ARRPreloadConfig(TIM8, ENABLE); //使能 TIM1 在 ARR 上的预装载寄存器
  TIM_CtrlPWMOutputs(TIM8, ENABLE);	//MOE 主输出使能,高级定时器必须开启
  TIM_Cmd(TIM8, ENABLE);				//使能 TIM1
}

/*
PE9  - TIM1_CH1 - pwm1
PE11 - TIM1_CH2 - pwm2
PE13 - TIM1_CH3 - pwm3
PE14 - TIM1_CH4 - pwm4

PC6  - TIM8_CH1 - pwm5
PC7  - TIM8_CH2 - pwm6
PC8  - TIM8_CH3 - pwm7
PC9  - TIM8_CH4 - pwm8
*/

void PWM_Init(u16 pwm1, u16 pwm2, u16 pwm3, u16 pwm4, u16 pwm5, u16 pwm6, u16 pwm7, u16 pwm8)
{
  TIM_OCInitTypeDef TIM_OCInitStructure;

  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;			 			  //PWM1 模式
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;

  /***********************************   TIM4   ***************************************/

  TIM_OCInitStructure.TIM_Pulse = pwm1;					  //设置待装入捕获比较寄存器的脉冲值
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //高电平有效
  TIM_OC1Init(TIM1, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
  TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);		  //CH1 预装载使能

  TIM_OCInitStructure.TIM_Pulse = pwm2;					  //设置待装入捕获比较寄存器的脉冲值
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //OC2 高电平有效
  TIM_OC2Init(TIM1, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
  TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);		  //CH2 预装载使能

  TIM_OCInitStructure.TIM_Pulse = pwm3;					  //设置待装入捕获比较寄存器的脉冲值
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //高电平有效
  TIM_OC3Init(TIM1, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
  TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);		  //CH3 预装载使能

  TIM_OCInitStructure.TIM_Pulse = pwm4;					  //设置待装入捕获比较寄存器的脉冲值
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //高电平有效
  TIM_OC4Init(TIM1, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
  TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Enable);		  //CH4 预装载使能

//  /***********************************   TIM8   ***************************************/

//  TIM_OCInitStructure.TIM_Pulse = pwm5;					  //设置待装入捕获比较寄存器的脉冲值
//  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //高电平有效
//  TIM_OC1Init(TIM8, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
//  TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);		  //CH1 预装载使能

//  TIM_OCInitStructure.TIM_Pulse = pwm6;					  //设置待装入捕获比较寄存器的脉冲值
//  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //高电平有效
//  TIM_OC2Init(TIM8, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
//  TIM_OC2PreloadConfig(TIM8, TIM_OCPreload_Enable);		  //CH2 预装载使能

//  TIM_OCInitStructure.TIM_Pulse = pwm7;					  //设置待装入捕获比较寄存器的脉冲值
//  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //OC1 高电平有效
//  TIM_OC3Init(TIM8, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
//  TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);		  //CH3 预装载使能

//  TIM_OCInitStructure.TIM_Pulse = pwm8;					  //设置待装入捕获比较寄存器的脉冲值
//  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //OC1 高电平有效
//  TIM_OC4Init(TIM8, &TIM_OCInitStructure);				  //根据指定的参数初始化外设 TIMx
//  TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);		  //CH4 预装载使能
}

void Set_PWM(int left, int right)
{

  if(left > 0)	PWMA2 = 0, PWMA1 =  left;
  else 	      PWMA1 = 0, PWMA2 = -1 * left;

  if(right > 0)	PWMB2 = 0, PWMB1 = right;
  else 	      PWMB1 = 0, PWMB2 = -1 * right;

}
