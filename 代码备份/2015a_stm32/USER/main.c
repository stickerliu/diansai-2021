#include "sys.h"
#include "delay.h"
#include "ADS1118.h"
#include "PWM.h"
#include "usart.h"
#include "KEY.h"
#include "oled.h"
#include "exti.h"
#include "pid.h"



int main(void)
{
	u8 display=0,tick=0,pid_state=0;
	
  SystemInit();
  delay_init();
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2

  EXTIX_Init();
  uart_init(115200);
  ADS1118GPIOInit();
  ADS1118Init(ADS1118_SS_NONE, ADS1118_MODE_LX, ADS1118_DR_128, ADS1118_PULL_UP_EN_E, ADS1118_NOP_W);
  tim1_pwm_init(7 - 1, 1000 - 1); //10kHz
  PID_Init();
	
	delay_ms(1000);


  OLED_Init();
  OLED_ColorTurn(0);//0正常显示，1 反色显示
  OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示
  OLED_Clear();
	
//	OLED_ShowChinese(46, 0, 1, 16);
//	OLED_ShowChinese(66, 0, 2, 16);
//	OLED_ShowChinese(86, 0, 3, 16);
//	OLED_ShowString(0, 20, "I1:", 16);
//	OLED_ShowString(52,20,".",16);
//	OLED_ShowString(104, 20, "A", 16);
//	OLED_ShowString(0, 40, "U1:", 16);
//	OLED_ShowString(52,40,".",16);
//	OLED_ShowString(104, 40, "V", 16);

  mode = sw_m;
//	DUTY=750;
//	PWM_OUT=1;
	key_state=NONE_PRES;
	
  while(1)
    {
//			Getdata(ADS1118_MUX_2G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//U1
//			u1=FV[2]*11.29;
//			Getdata(ADS1118_MUX_3G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//I1
//			i1=(FV[3]-23.46)/1.693;
//			printf("*** %f *** %f ***\r\n",u1,i1);
//			Getdata(ADS1118_MUX_0G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//U1
//			printf("************ %f ************\r\n",FV[0]);

      switch(mode)
        {
        case sw_m://模式选择
					OLED_Clear();
					PWM_OUT=0;
					pid_state=0;
					TIM_Cmd(TIM6, DISABLE);
					while(key_state!=KEY2_PRES)
					{
						if(key_state==KEY0_PRES)
						{
							display++;
							key_state=NONE_PRES;
						}
						
						if(display==4)
						{
							display=0;
						}
						
						switch(display)
						{
							case 0:
								OLED_Clear();
								OLED_ShowChinese(26, 0, 0, 16);
								OLED_ShowChinese(46, 0, 1, 16);
								OLED_ShowChinese(66, 0, 2, 16);
								OLED_ShowChinese(86, 0, 3, 16);//充电模式
								OLED_Refresh();
								mode=base_m;		
								DUTY=800;
								break;
							
							case 1:
								OLED_Clear();
								OLED_ShowChinese(26, 0, 4, 16);
								OLED_ShowChinese(46, 0, 1, 16);
								OLED_ShowChinese(66, 0, 2, 16);
								OLED_ShowChinese(86, 0, 3, 16);//放电模式
								OLED_Refresh();
								mode=extra_1;
								break;
							
							case 2:
								OLED_Clear();
								OLED_ShowChinese(26, 0, 9, 16);
								OLED_ShowChinese(46, 0, 10, 16);
								OLED_ShowChinese(66, 0, 2, 16);
								OLED_ShowChinese(86, 0, 3, 16);//发挥模式
								OLED_Refresh();
								mode=extra_2;
								break;
							
							case 3:
								OLED_Clear();
								OLED_ShowChinese(26, 0, 11, 16);
								OLED_ShowChinese(46, 0, 12, 16);
								OLED_ShowChinese(66, 0, 2, 16);
								OLED_ShowChinese(86, 0, 3, 16);//调试模式
								OLED_Refresh();
							  mode=debug_m;
								break;
							
							default:
								break;
						}
					}
					key_state=NONE_PRES;
          break;

        case base_m:
					while(key_state!=KEY2_PRES)
					{
						//OLED_Clear();
						
						PWM_OUT=1;
						
						Getdata(ADS1118_MUX_2G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//U1
						u1=FV[2]*11;
						Getdata(ADS1118_MUX_3G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//I1
						i1=(FV[3]-23.46)/1.693-1;
						
						OLED_ShowChinese(26, 0, 0, 16);//充
						OLED_ShowChinese(46, 0, 1, 16);
						OLED_ShowChinese(66, 0, 2, 16);
						OLED_ShowChinese(86, 0, 3, 16);
						OLED_ShowString(0, 20, "I1:", 16);
						OLED_ShowString(52,20,".",16);
						OLED_ShowString(104, 20, "A", 16);
						OLED_ShowString(0, 40, "U1:", 16);
						OLED_ShowString(52,40,".",16);
						OLED_ShowString(104, 40, "V", 16);
						show(i1/1000, u1/1000-0.6);
						
//						//target_i=1600;
//						tick++;
//						//printf("%d \r\n",tick);
//						if(tick/15==1)
//						{
//							target_i=1000;
//						}
//						if(tick/15==2)
//						{
//							target_i=1000;
//							tick=0;
//						}
						
						if(key_state == KEY1_PRES )
						{
							
							pid_state=!pid_state;
							target_i=1000;
							TIM_Cmd(TIM6, pid_state);//开启或关闭定时器
							key_state=NONE_PRES;
						}
						
						if(key_state == KEY0_PRES )
						{
							target_i+=100;
							key_state=NONE_PRES;
							if(target_i == 2100)
							{
								target_i=1000;
							}
						}
						
						if(u1>25000&&pid_state==1)
						{
							key_state=KEY2_PRES;
						}
						
						
					}
					mode=sw_m;
					key_state=NONE_PRES;
						
          
          break;

        case extra_1:
					while(key_state!=KEY2_PRES)
					{
						PWM_OUT=1;
						//TIM_Cmd(TIM6, ENABLE);					   //开启定时器
						
						Getdata(ADS1118_MUX_1G, ADS1118_PGA_40, ADS1118_TS_MODE_ADC, CS_0);//U2
						u2=FV[1]*11.7005;
						Getdata(ADS1118_MUX_2G, ADS1118_PGA_40, ADS1118_TS_MODE_ADC, CS_0);//U1
						u1=FV[2]*11.0137;
						
						OLED_ShowChinese(26, 0, 4, 16);//放
						OLED_ShowChinese(46, 0, 1, 16);
						OLED_ShowChinese(66, 0, 2, 16);
						OLED_ShowChinese(86, 0, 3, 16);
						OLED_ShowString(0, 20, "U1:", 16);
						OLED_ShowString(52,20,".",16);
						OLED_ShowString(104, 20, "V", 16);
						OLED_ShowString(0, 40, "U2:", 16);
						OLED_ShowString(52,40,".",16);
						OLED_ShowString(104, 40, "V", 16);
						show(u1/1000, u2/1000-0.8);
						
						if(key_state == KEY1_PRES )
						{
							pid_state=!pid_state;
							target_v=3000;
							TIM_Cmd(TIM6, pid_state);//开启或关闭定时器
							key_state=NONE_PRES;
						}
						
					}
					mode=sw_m;
					key_state=NONE_PRES;
          break;

				 case extra_2:
					while(key_state!=KEY2_PRES)
					{
						PWM_OUT=1;
						//TIM_Cmd(TIM6, ENABLE);					   //开启定时器
						
						Getdata(ADS1118_MUX_1G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//U2
						u2=FV[1]/100;
						Getdata(ADS1118_MUX_3G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);//I1
						i1=(FV[3] -23.46)/ 1693+0.039;
						
						OLED_ShowChinese(26, 0, 4, 16);//放
						OLED_ShowChinese(46, 0, 1, 16);
						OLED_ShowChinese(66, 0, 2, 16);
						OLED_ShowChinese(86, 0, 3, 16);
						OLED_ShowString(0, 20, "I1:", 16);
						OLED_ShowString(52,20,".",16);
						OLED_ShowString(104, 20, "A", 16);
						OLED_ShowString(0, 40, "U2:", 16);
						OLED_ShowString(52,40,".",16);
						OLED_ShowString(104, 40, "V", 16);
						show(i1, u2);
					}
					mode=sw_m;
					key_state=NONE_PRES;
          break;
					
				 case debug_m:
					while(key_state!=KEY2_PRES)
					{
						OLED_ShowChinese(26, 0, 11, 16);//放
						OLED_ShowChinese(46, 0, 12, 16);
						OLED_ShowChinese(66, 0, 2, 16);
						OLED_ShowChinese(86, 0, 3, 16);
						OLED_ShowString(0, 20, "U2:", 16);
						OLED_ShowString(52,20,".",16);
						OLED_ShowString(104, 20, "A", 16);
						Getdata(ADS1118_MUX_0G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);
						Getdata(ADS1118_MUX_3G, ADS1118_PGA_61, ADS1118_TS_MODE_ADC, CS_0);
						show(FV[0] / 1000, FV[3] / 1000);
					}
					mode=sw_m;
					key_state=NONE_PRES;
          break;

        default:

          break;
        }


    }

}
