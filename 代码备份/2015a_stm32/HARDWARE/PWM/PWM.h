#ifndef __PWM_H
#define __PWM_H 		

#include "sys.h"

#define DUTY TIM1->CCR3

void tim1_pwm_init(u16 psc,u16 arr);

#define PWM_OUT PEout(5)

#endif 
