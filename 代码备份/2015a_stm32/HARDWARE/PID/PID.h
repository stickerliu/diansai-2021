#ifndef __PID_H
#define __PID_H
#include "sys.h"

/*
模式切换
基础
发挥1
发挥2
调试
*/

typedef enum
{
	sw_m=0,
	base_m,
	extra_1,
	extra_2,
	debug_m
}mode_select;

extern u8 niming[10],mode;
extern int target_v,target_i;
extern float i1,u1,u2;

void PID_Init(void);
int BASE_PID(int En, int Tar);
void Xianfu_Pwm(void);



#endif
